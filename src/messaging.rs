// commands.rs
//
// Copyright 2022 waimus <onemorelight.protophyta@aleeas.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

pub fn help(command: &str) {
    match command {
        "default" | "help" => {
            println!("Usage:\n  \'catpr COMMAND [ARGS...]\'");
            println!("Commmands:");
            println!("  help\tShow help");
            println!("  ls\tShow number of task(s) to be done");
            println!("  list\tList tasks");
            println!("  add\tAdd task. Usage \'add [SUMMARY] [DESCRIPTION]\'");
            println!("  edit\tEdit task. Usage\'edit [ID] [SUMMARY] [DESCRIPTION]\'");
            println!("  rm\tRemove task. Usage \'rm [ID]\'");
            println!("Use \'catpr help [COMMAND]\' to see command-specific help")
        }
        "add" => {
            println!("Summary: Add new task");
            println!("Usage:\n  \'catpr add [SUMMARY] [DESCRIPTION]\'");
        }
        "ls" => {
            println!("Summary: Show number of task(s) to be done");
            println!("Usage:\n  \'catpr ls\'");
        }
        "list" => {
            println!("Summary: List the complete task list");
            println!("Usage:\n  \'catpr list [OPTIONS]\'");
            println!(
                "Options:\n  -c, --count\n  Show only number of task(s), same as \'catpr ls\'"
            );
        }
        "edit" => {
            println!("Summary: Edit existing task with new summary and description");
            println!("Usage:\n  \'catpr edit [ID] [NEW SUMMARY] [NEW DESCRIPTION]\'");
            println!("Options:\n --key\t\t Only edit key of selected ID");
            println!(" --value\t Only edit value of selected ID");
        }
        "rm" | "remove" => {
            println!("Summary: Remove a task from the list");
            println!("Usage:\n  \'catpr rm [ID] or [OPTIONS]\'");
            println!("Options:\n  -a, --all\n  Remove all entries in the list");
        }
        unknown => {
            err_invalid_command(&unknown.to_string());
        }
    }
}

pub fn print_entries(id: i32, key: &str, value: &String, key_width: usize) {
    if id == 1 {
        println!("Task(s) to be done:");
    }
    println!(
        "  [{}]\t{:<key_width$} :  {}",
        id,
        key.to_uppercase(),
        value
    );
}

pub fn short_list(count: i32) {
    if count > 0 {
        println!("You have {} task(s) to be done", count);
        println!("Use \'catpr list\' to show the full list");
    } else {
        info_no_entries_show();
    }
}

fn print_error(error: String, hint: String) {
    println!("ERROR: {}\n{}", error, hint);
}

pub fn err_invalid_command(arg: &String) {
    let error: String = format!("\'{}\' is an unrecognized command", arg);
    let hint: String = "Use \'catpr help\' to see available commands".to_string();
    print_error(error, hint);
}

pub fn err_invalid_option(opt: &String) {
    let error: String = format!("\'{}\' is an unrecognized option", opt);
    let hint: String = "Use \'catpr help\' to see available commands".to_string();
    print_error(error, hint);
}

pub fn err_no_commands() {
    let error: String = "No command passed".to_string();
    let hint: String = "Use \'catpr help\' to see available commands".to_string();
    print_error(error, hint);
}

pub fn err_id_as_zero() {
    let error: String = "Assigned ID cannot be 0".to_string();
    let hint: String = "Use \'catpr list\' to show the full list".to_string();
    print_error(error, hint);
}

pub fn err_out_of_bound() {
    let error: String = "Assigned ID is out of array bound".to_string();
    let hint: String = "Use \'catpr list\' to show the full list".to_string();
    print_error(error, hint);
}

pub fn info_entry_written(key: &String) {
    println!("New task \'{}\' is saved", key);
}

pub fn info_entry_modified(id: i32) {
    println!("Task with ID {} has been modified", id);
}

pub fn info_entry_removed(id: i32) {
    println!("Task with ID {} has been removed", id);
}

pub fn info_no_entries_show() {
    println!("No task to be done");
    println!("Use \'catpr add [SUMMARY] [DESCRIPTION]\' to add new task");
}

pub fn info_no_entries_remove() {
    println!("No task to be removed");
    println!("Use \'catpr add [SUMMARY] [DESCRIPTION]\' to add new task");
}
