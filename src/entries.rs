// entries.rs
//
// Copyright 2022 waimus <onemorelight.protophyta@aleeas.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod data;
mod ospath;
use crate::messaging;
use data::{EntryItem, EntryObject};
use std::path::{Path, PathBuf};

/// Show entries count
pub fn list_short_entries() {
    let json_path: PathBuf = ospath::get_data_path().unwrap();
    messaging::short_list(EntryObject::get_entries_count(&json_path).unwrap());
}

/// Parse a JSON object and list its content
pub fn list_entries() {
    let json_path: PathBuf = ospath::get_data_path().unwrap();
    if EntryObject::get_entries_count(&json_path).unwrap() > 0 {
        if Path::new(&json_path).exists() {
            let entry_object: EntryObject = EntryObject::from_json(&json_path).unwrap().rearrange_item_ids();

            // Get the longest key string length for its column width
            let longest_key: usize = entry_object
                .items
                .iter()
                .map(|x| x.clone().key().capacity())
                .max()
                .unwrap_or(0);

            entry_object
                .items
                .into_iter()
                .for_each(|x| messaging::print_entries(*x.id(), x.key(), x.value(), longest_key));
        }
    } else {
        // Inform user if there's no entries to show
        messaging::info_no_entries_show();
    }
}

/// Create a struct with assigned params and write it into the JSON file.
pub fn write_entry(entry_key: &String, entry_value: &String) -> Result<(), std::io::Error> {
    let json_path: PathBuf = ospath::get_data_path().unwrap();
    let id: i32 = EntryObject::get_entries_count(&json_path).unwrap() + 1;

    // If file already exist, append inside object
    let entry_object: EntryObject = if Path::new(&json_path).exists() {
        EntryObject::from_json(&json_path)
            .unwrap()
            .push_create(
                id, 
                entry_key.to_string(), 
                entry_value.to_string())
            .rearrange_item_ids()
    }
    // Else create new object
    else {
        EntryObject::new(
            vec![
                EntryItem::new(
                    id,
                    entry_key.to_string(),
                    entry_value.to_string()
        )])
    };
    // Finally write to file
    std::fs::write(
        &json_path,
        serde_json::to_string_pretty(&entry_object).unwrap(),
    )?;
    messaging::info_entry_written(entry_key);
    Ok(())
}

/// Edits entry key given the passed argument
pub fn edit_entry_key(id: i32, new_key: &String) -> Result<(), std::io::Error> {
    let json_path: PathBuf = ospath::get_data_path().unwrap();

    if id < 0 {
        // ID cannot be 0
        messaging::err_id_as_zero();
    } else if id <= EntryObject::get_entries_count(&json_path).unwrap() {
        if Path::new(&json_path).exists() {
            let current_entry_object: EntryObject = EntryObject::from_json(&json_path).unwrap();

            // Retrieve not modified value
            let current_entry_value: String =
                current_entry_object.items[id as usize - 1].value().clone();

            // Create new entry with the new properties
            let entry_item: EntryItem =
                EntryItem::new(id, new_key.to_string(), current_entry_value);
            let mut entry_object: EntryObject = EntryObject::from_json(&json_path).unwrap();

            // Change current entry with given new entry
            entry_object.items[id as usize - 1] = entry_item;

            // Update JSON file content as long as the entries length above 0
            if !entry_object.items.is_empty() {
                // Save modified object into file
                std::fs::write(
                    &json_path,
                    serde_json::to_string_pretty(&entry_object).unwrap(),
                )?;
            }
        }
    } else if EntryObject::get_entries_count(&json_path).unwrap() == -1 {
        // If file is non-existent
        messaging::info_no_entries_show();
    } else {
        messaging::err_out_of_bound();
    }

    Ok(())
}

/// Edits entry value given the passed argument
pub fn edit_entry_value(id: i32, new_value: &String) -> Result<(), std::io::Error> {
    let json_path: PathBuf = ospath::get_data_path().unwrap();

    if id < 1 {
        // ID cannot be 0
        messaging::err_id_as_zero();
    } else if id <= EntryObject::get_entries_count(&json_path).unwrap() {
        if Path::new(&json_path).exists() {
            let current_entry_object: EntryObject = EntryObject::from_json(&json_path).unwrap();

            // Retrieve not modified key
            let current_entry_key: String =
                current_entry_object.items[id as usize - 1].key().clone();

            // Create new entry with the new properties
            let entry_item: EntryItem =
                EntryItem::new(id, current_entry_key, new_value.to_string());
            let mut entry_object: EntryObject = EntryObject::from_json(&json_path).unwrap();

            // Change current entry with given new entry
            entry_object.items[id as usize - 1] = entry_item;

            // Update JSON file content as long as the entries length above 0
            if !entry_object.items.is_empty() {
                // Save modified object into file
                std::fs::write(
                    &json_path,
                    serde_json::to_string_pretty(&entry_object).unwrap(),
                )?;
            }
        }
    } else if EntryObject::get_entries_count(&json_path).unwrap() == -1 {
        // If file is non-existent
        messaging::info_no_entries_show();
    } else {
        messaging::err_out_of_bound();
    }
    Ok(())
}

/// Remove an entry according to its ID
pub fn remove_entry(id: i32) -> Result<(), std::io::Error> {
    let json_path: PathBuf = ospath::get_data_path().unwrap();

    if id < 1 {
        // ID cannot be 0
        messaging::err_id_as_zero();
    } else if id <= EntryObject::get_entries_count(&json_path).unwrap() {
        if Path::new(&json_path).exists() {
            // Load object from the JSON file
            // Find and remove element given the ID
            // Finally update IDs of every entries to the right index
            let entry_object: EntryObject = EntryObject::from_json(&json_path)
                .unwrap()
                .remove_at(id as usize - 1)
                .rearrange_item_ids();

            messaging::info_entry_removed(id);

            // Update JSON file content as long as the entries length above 0
            if !entry_object.items.is_empty() {
                // Save modified object into file
                std::fs::write(
                    &json_path,
                    serde_json::to_string_pretty(&entry_object).unwrap(),
                )?;
            }
            // If items lenght ended up being 0
            else {
                remove_json_file().unwrap();
            }
        }
    } else if EntryObject::get_entries_count(&json_path).unwrap() == -1 {
        // If file is non-existent
        messaging::info_no_entries_remove();
    } else {
        messaging::err_out_of_bound();
    }
    Ok(())
}

/// Remove all entries by removing the JSON file
pub fn remove_json_file() -> Result<(), std::io::Error> {
    let json_path: PathBuf = ospath::get_data_path().unwrap();
    if EntryObject::get_entries_count(&json_path).unwrap() > 0 {
        // Check if the file exists then remove it
        if Path::new(&json_path).exists() {
            std::fs::remove_file(&json_path)?;
        }
    } else {
        messaging::info_no_entries_remove();
    }
    Ok(())
}
