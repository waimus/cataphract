// main.rs
//
// Copyright 2022 waimus <onemorelight.protophyta@aleeas.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod entries;
mod messaging;
use std::{cmp::Ordering, env};

fn main() {
    let mut args: Vec<String> = env::args().collect();
    parse_arguments(&mut args);
}

fn parse_arguments(arguments: &mut Vec<String>) {
    if arguments.len() > 1 {
        // Command is always at index 1
        let command: &String = &arguments[1];

        match command.to_lowercase().as_str() {
            // SHORT LIST: ls
            "ls" => {
                entries::list_short_entries();
            }
            // LIST: list [OPTIONS]
            // OPTIONS: [null] [--count] [-c]
            "list" => {
                if arguments.len() > 2 {
                    // Handle --count and -c option
                    if arguments[2] == "--count" || arguments[2] == "-c" {
                        entries::list_short_entries();
                    } else {
                        messaging::err_invalid_option(&arguments[2]);
                    }
                } else {
                    // Else without options, do normal listing
                    entries::list_entries();
                }
            }
            // RM: rm [OPTIONS]
            // OPTIONS: [--all] [-a] [id]
            "rm" | "remove" => {
                if arguments.len() > 2 {
                    // Option to remove all entries
                    if arguments[2] == "--all" || arguments[2] == "-a" {
                        entries::remove_json_file().unwrap();
                    }
                    // Option to remove specified entry from ID
                    else {
                        let id: i32 = arguments[2].parse::<i32>().unwrap();
                        entries::remove_entry(id).unwrap();
                    }
                } else {
                    // Inform no id given
                    messaging::help(command.as_str());
                }
            }
            // ADD: add [SUMMARY] [DESCRIPTION]
            "add" => {
                // Check if arguments vector length is valid
                if arguments.len() > 2 {
                    entries::write_entry(&arguments[2], &arguments[3]).unwrap()
                } else {
                    // Inform no key and value given
                    messaging::help(command.as_str());
                }
            }
            // EDIT: edit [id] [SUMMARY] [DESCRIPTION]
            "edit" => {
                match arguments.len().cmp(&4) {
                    Ordering::Greater => {
                        let id: i32 = arguments[2].parse::<i32>().unwrap();
                        // Edit specific option
                        if arguments[3] == "--key" {
                            entries::edit_entry_key(id, &arguments[4]).unwrap();
                        } else if arguments[3] == "--value" {
                            entries::edit_entry_value(id, &arguments[4]).unwrap();
                        } else {
                            // Else edit both key & value
                            entries::edit_entry_key(id, &arguments[3]).unwrap();
                            entries::edit_entry_value(id, &arguments[4]).unwrap();
                        }
                        messaging::info_entry_modified(id);
                    }
                    Ordering::Equal => {
                        // Edit only key when inputs has only one value passed
                        let id: i32 = arguments[2].parse::<i32>().unwrap();
                        entries::edit_entry_key(id, &arguments[3]).unwrap();
                        messaging::info_entry_modified(id);
                    }
                    Ordering::Less => {
                        // Inform no new key and value given
                        messaging::help(command.as_str());
                    }
                }
            }
            // HELP: help [OPTIONS]
            "help" => {
                if arguments.len() > 2 {
                    messaging::help(arguments[2].as_str());
                } else {
                    messaging::help("default");
                }
            }
            _ => {
                messaging::err_invalid_command(command);
            }
        }
    } else {
        messaging::err_no_commands();
    }
}
