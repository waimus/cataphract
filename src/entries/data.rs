// data.rs
//
// Copyright 2022 waimus <onemorelight.protophyta@aleeas.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use serde_derive::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

#[derive(Deserialize, Serialize, Clone)]
pub struct EntryItem {
    id: i32,
    key: String,
    value: String,
}

impl EntryItem {
    /// Create new item from given paramenters
    pub fn new(entry_id: i32, entry_key: String, entry_value: String) -> Self {
        Self {
            id: entry_id,
            key: entry_key,
            value: entry_value,
        }
    }

    // Implement public getter of struct members
    pub fn id(&self) -> &i32 {
        &self.id
    }
    pub fn key(&self) -> &String {
        &self.key
    }
    pub fn value(&self) -> &String {
        &self.value
    }
}

impl Iterator for EntryItem {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.id)
    }
}

#[derive(Deserialize, Serialize, Default)]
pub struct EntryObject {
    pub items: Vec<EntryItem>,
}

impl EntryObject {
    /// Create new object from JSON file
    pub fn from_json(json_path: &PathBuf) -> Result<Self, std::io::Error> {
        let mut obj: EntryObject = EntryObject::default();
        if Path::new(&json_path).exists() {
            let file = std::fs::read_to_string(json_path)?;
            obj = serde_json::from_str(&file).unwrap_or_else(|e| {
                panic!("Something wrong with the JSON file (JSON ERROR: {})", e)
            });
        }
        Ok(Self { items: obj.items })
    }

    /// Create new object from existing items
    pub fn new(items: Vec<EntryItem>) -> EntryObject {
        EntryObject { items }
    }

    /// Push new item into vector
    pub fn push_item(&mut self, item: EntryItem) -> EntryObject {
        self.items.push(item);
        EntryObject::new(self.items.clone())
    }

    /// Push new item by creating new EntryItem
    pub fn push_create(&mut self, id: i32, key: String, value: String) -> EntryObject {
        let new_entry = EntryItem::new(id, key, value);
        self.push_item(new_entry)
    }

    /// Remove item from vector given the index
    pub fn remove_at(&mut self, index: usize) -> EntryObject {
        self.items.remove(index);
        EntryObject::new(self.items.clone())
    }

    /// Return entry count of a JSON file
    pub fn get_entries_count(json_path: &PathBuf) -> Result<i32, std::io::Error> {
        let count: i32;
        let obj: EntryObject;
        if Path::new(&json_path).exists() {
            let file = std::fs::read_to_string(json_path)?;
            obj = serde_json::from_str(&file).unwrap_or_else(|e| {
                panic!("Something wrong with the JSON file (JSON ERROR: {})", e)
            });
            count = obj.items.len() as i32;
        } else {
            // Return -1 if file is non-existent
            count = -1;
        }
        Ok(count)
    }

    /// Function to rearrange ID of every item in an object
    pub fn rearrange_item_ids(&mut self) -> EntryObject {
        for (idx, element) in self.items.iter_mut().enumerate() {
            element.id = idx as i32 + 1;
        }

        EntryObject::new(self.items.clone())
    }
}
