// ospath.rs
//
// Copyright 2022 waimus <onemorelight.protophyta@aleeas.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use directories::ProjectDirs;
use std::path::PathBuf;

const JSON_FILENAME: &str = "cataphract_entries.json";

const QUAL: &str = "com";
const ORG: &str = "waimus";
const APP: &str = "cataphract";

pub fn get_data_path() -> Option<PathBuf> {
    let mut path: PathBuf = PathBuf::new();

    if let Some(project_directory) = ProjectDirs::from(QUAL, ORG, APP) {
        // Get $HOME/.local/share path on Linux
        // See https://github.com/dirs-dev/directories-rs to see other OS equivalent
        path = project_directory.data_dir().to_path_buf();

        if !project_directory.data_dir().exists() {
            // Create directory from path if it's not exist
            std::fs::create_dir_all(&path).expect("Failed to create directory!");
            println!("Cataphract data was not found. Created new data at {}", &path.to_string_lossy());
        }
    }
    path.push(JSON_FILENAME);
    Some(path)
}
