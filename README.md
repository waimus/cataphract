# Cataphract
![](https://media.mstdn.social/media_attachments/files/108/567/335/113/235/381/original/3959a5613b0c8c7b.png)

A minimal to-do/reminder list CLI app written in Rust. Simply save an entry with summary and description, Cataphract does not have further fancy stuff like task tracking.

#### Video Demo
View on [YouTube](https://www.youtube.com/watch?v=OTBEw7UG37Y) or [Invidious](https://invidious.snopyta.org/watch?v=OTBEw7UG37Y). 

## Compiling
Cataphract uses Cargo.

Dependencies:
 - `serde = "1.0.117"`
 - `serde_derive = "1.0.117"`
 - `serde_json = "1.0.59"`
 - `directories = "4.0"`

See the `Cargo.toml` file to see project metadata.

To build:

```sh
cargo build
```
Run with `cargo run`, built binary is in the `target/debug/catpr` directory once it's built. 

To quick install, symlink the built binary to `~/.local/bin` (e.g. at project root dir, do `ln -s $(pwd)/target/debug/catpr ~/.local/bin/catpr`). Then you can run Cataphract by calling `catpr` from terminal.

## Help
Run `./target/debug/catpr help` or `cargo run help` to see available commands.

## Background
I need a super-simple to-do list tool that I can bind into my `.zshrc` startup so it'll appear everytime I open terminal.

Why "Cataphract"? I like the Byzantines in Age of Empires II and it sounds cool. 

### How it works
Cataphract simply reads/write entries from/into a JSON file. Each entry have a property of ID, Key, and Value. 

JSON data saving path is handled with [`directories`](https://github.com/dirs-dev/directories-rs) to hopefully get it work with not only Linux, but also macOS and Windows.

By how Cataphract works, this can actually be modified into various tools which I do have an idea for something else but I guess I'll start later.

## License
GPLv3.0 or later, see [LICENSE](./LICENSE)
